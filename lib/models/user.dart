class User {
  final int UserID;
  final String Username;
  final String Email;
  final String Password;
  final String PhotoUrl;


  User({required this.UserID, required this.Username, required this.Email, required this.Password, required this.PhotoUrl});




  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      UserID: json['UserID'],
      Username: json['Username'],
      Email: json['Email'],
      Password: json['Password'],
      PhotoUrl: json['PhotoUrl'],
    );
  }
}