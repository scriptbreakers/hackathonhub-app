import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

Widget textFild({
  required String hintTxt,
  required TextEditingController controller,
  IconData? suffixIcon, // Added parameter for the suffix icon
  bool isObscureText = false,
  TextInputType? keyboardType,
  void Function(bool)? onToggleVisibility, // Added callback for visibility toggle
}) {
  IconData toggleIcon =
  isObscureText ? CupertinoIcons.eye_slash_fill : CupertinoIcons.eye_fill;

  return Container(
    height: 55,
    padding: EdgeInsets.symmetric(horizontal: 10.0),
    margin: EdgeInsets.symmetric(
      horizontal: 10.0,
      vertical: 8.0,
    ),
    decoration: BoxDecoration(
      color: Colors.white60,
      borderRadius: BorderRadius.circular(20.0),
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          width: 250.0,
          child: TextField(
            controller: controller,
            textAlignVertical: TextAlignVertical.center,
            obscureText: isObscureText,
            keyboardType: keyboardType,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: hintTxt,
            ),
          ),
        ),
        if (suffixIcon != null && hintTxt == 'Password')
          InkWell(
            onTap: () {
              if (onToggleVisibility != null) {
                onToggleVisibility(!isObscureText);
              }
            },
            child: Icon(
              toggleIcon, // Use toggleIcon to display the appropriate icon
              color: Colors.black87,
            ),
          ),
        if (hintTxt == 'Email')
          Icon(
            CupertinoIcons.mail_solid,
            color: Colors.black87,
          ),

        if (hintTxt == 'User')
          Icon(
            CupertinoIcons.person_alt_circle_fill,
            color: Colors.black87,
          ),
      ],
    ),
  );
}
