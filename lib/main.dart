import 'package:flutter/material.dart';
import 'package:hackathonhub/Pages/Splash_Page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: 'Epilogue'),
      home: HomePage(),
    );
  }
}
