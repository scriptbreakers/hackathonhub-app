import 'package:flutter/material.dart';
import 'login_page.dart';
import 'package:hackathonhub/main_button.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    final screenHeight = mediaQuery.size.height;

    final gradient = LinearGradient(
      colors: [Colors.black, Colors.blueGrey],
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter,
    );

    return Scaffold(
      body: Container(
        decoration: BoxDecoration(gradient: gradient),
        child: Stack(
          children: [
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                height: screenHeight / 2,
                width: double.infinity,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    RichText(
                      text: TextSpan(children: [
                        TextSpan(
                          text: 'HackathonHub',
                          style: TextStyle(
                            fontFamily: 'Epilogue',
                            fontSize: 40,
                            color: Color(0xffedf2f3),
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ]),
                    ),
                    SizedBox(
                      height: 50,
                      child: Text(
                        'Find your perfect buddy',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: 'Epilogue',
                          color: Color(0xffbedf2f3),
                          fontSize: 15,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                    Center(
                      child: Image.asset(
                        'assets/images/i.png',
                        height: 160,
                        width: 160,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 80.0),
              child: Align(
                alignment: Alignment
                    .bottomCenter, // Align the button to the bottom center
                child: Mainbutton(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (builder) => const LoginPage(),
                      ),
                    );
                  },
                  btnColor: Colors.lightBlueAccent.withOpacity(0.5),
                  text: 'Get Started',
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
