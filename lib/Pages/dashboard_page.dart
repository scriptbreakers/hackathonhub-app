import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hackathonhub/Pages/navbar_view/chat_view.dart';
import 'package:hackathonhub/Pages/navbar_view/feed_view.dart';
import 'package:hackathonhub/Pages/navbar_view/notif_view.dart';
import 'package:hackathonhub/Pages/navbar_view/profile_view.dart';
import 'package:persistent_bottom_nav_bar/persistent_tab_view.dart';
import 'package:google_nav_bar/google_nav_bar.dart';

class DashboardPage extends StatefulWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  State<DashboardPage> createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  final controller = PersistentTabController(initialIndex: 0);
  List<Widget> _buildScreen() {
    return [
      FeedView(),
      // ChatView(),
      // NotifView(),
      ProfileView(),
    ];
  }

  List<PersistentBottomNavBarItem> _navBarItem(){
    return[
      PersistentBottomNavBarItem(
        icon: Icon(CupertinoIcons.home, size: 24),
        title: ("Home"),
        activeColorPrimary: CupertinoColors.activeBlue,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),

      // PersistentBottomNavBarItem(
      //   icon: Icon(CupertinoIcons.chat_bubble_2),
      //   title: ("Chats"),
      //   activeColorPrimary: CupertinoColors.activeBlue,
      //   inactiveColorPrimary: CupertinoColors.systemGrey,
      // ),
      //
      // PersistentBottomNavBarItem(
      //   icon: Icon(CupertinoIcons.bell),
      //   title: ("Notifications"),
      //   activeColorPrimary: CupertinoColors.activeBlue,
      //   inactiveColorPrimary: CupertinoColors.systemGrey,
      // ),

      PersistentBottomNavBarItem(
        icon: Icon(CupertinoIcons.person),
        title: ("Profile"),
        activeColorPrimary: CupertinoColors.activeBlue,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),

    ];
  }

  @override
  Widget build(BuildContext context) {
    return PersistentTabView(
      context,
      screens: _buildScreen(),
      items: _navBarItem(),
      controller: controller,
      backgroundColor: Colors.white60,
      decoration: NavBarDecoration(

          colorBehindNavBar: Colors.white70,
          borderRadius: BorderRadius.circular(15)),
      navBarStyle: NavBarStyle.style12, // 1, 12, 13, 6, 3
    );
  }
}























      /*
      bottomNavigationBar: Container(
        color: Colors.black,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
          child: GNav(
            backgroundColor: Colors.black,
              color: Colors.white70,
              activeColor: Colors.white70,
              tabBackgroundColor: Colors.grey.shade800,
              gap: 8,
              onTabChange: (value) {
              controller.index.value = value;
              },
              padding: EdgeInsets.all(16),
              tabs: const[
            GButton(
              icon: Icons.feed,
              text: 'Home',
            ),
            GButton(
              icon: Icons.message,
              text: 'Chat',
            ),
            GButton(
              icon: Icons.notifications,
              text: 'Notif',
            ),
            GButton(
              icon: Icons.person,
              text: 'Profile',
            ),
          ]),
        ),
      ),


    );
  }
}

       */
