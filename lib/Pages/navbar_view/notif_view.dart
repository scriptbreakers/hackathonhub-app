import 'package:flutter/material.dart';

class NotifView extends StatefulWidget {
  const NotifView({Key? key}) : super(key: key);

  @override
  State<NotifView> createState() => _NotifViewState();
}

class _NotifViewState extends State<NotifView> {
  List<NotificationItem> notifications = [
    NotificationItem(
      title: 'New Message',
      message: 'You have a new message from John Doe.',
    ),
    NotificationItem(
      title: 'Friend Request',
      message: 'Alice sent you a friend request.',
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Notifications',
          style: TextStyle(
            fontFamily: 'Epilogue',
            fontSize: 25,
            color: Color.fromARGB(255, 0, 0, 0),
            fontWeight: FontWeight.w600,
          ),
        ),
        backgroundColor: Color.fromARGB(255, 255, 255, 255),
        elevation: 0,
      ),
      body: ListView.builder(
        itemCount: notifications.length,
        itemBuilder: (context, index) {
          return NotificationCard(notification: notifications[index]);
        },
      ),
    );
  }
}

class NotificationItem {
  final String title;
  final String message;

  NotificationItem({
    required this.title,
    required this.message,
  });
}

class NotificationCard extends StatelessWidget {
  final NotificationItem notification;

  NotificationCard({required this.notification});

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      child: ListTile(
        leading: Icon(Icons.notifications),
        title: Text(notification.title),
        subtitle: Text(notification.message),
        onTap: () {
          // Navigate to a new screen when a notification is tapped
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) =>
                  NotificationDetails(notification: notification),
            ),
          );
        },
      ),
    );
  }
}

class NotificationDetails extends StatelessWidget {
  final NotificationItem notification;

  const NotificationDetails({required this.notification});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Notification Details',
          style: TextStyle(
            fontFamily: 'Epilogue',
            fontSize: 25,
            color: Color.fromARGB(255, 0, 0, 0),
            fontWeight: FontWeight.w400,
          ),
        ),
        backgroundColor: Color.fromARGB(255, 238, 237, 237),
        elevation: 0,
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Title:',
              style: TextStyle(
                fontFamily: 'Epilogue',
                fontSize: 20,
                color: Color.fromARGB(255, 0, 0, 0),
                fontWeight: FontWeight.w300,
              ),
            ),
            Text(notification.title),
            SizedBox(height: 10),
            Text(
              'Message:',
              style: TextStyle(
                fontFamily: 'Epilogue',
                fontSize: 20,
                color: Color.fromARGB(255, 0, 0, 0),
                fontWeight: FontWeight.w300,
              ),
            ),
            Text(notification.message),
          ],
        ),
      ),
    );
  }
}
