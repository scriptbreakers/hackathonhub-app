import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io' as io;
import 'dart:io';

class Post {
  final String username;
  String text;
  final String imageAsset;

  Post({
    required this.username,
    required this.text,
    required this.imageAsset,
  });

  int likes = 0;
  List<Comment> comments = [];
}

class Comment {
  final String username;
  final String text;

  Comment({required this.username, required this.text});
}

class FeedView extends StatefulWidget {
  const FeedView({Key? key}) : super(key: key);

  @override
  State<FeedView> createState() => _FeedViewState();
}

class _FeedViewState extends State<FeedView> {
  List<Post> posts = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Feed',
          style: TextStyle(
            fontFamily: 'Epilogue',
            fontSize: 25,
            color: Colors.black,
            fontWeight: FontWeight.w600,
          ),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: ListView.builder(
        itemCount: posts.length,
        itemBuilder: (context, index) {
          return PostWidget(post: posts[index]);
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.blueGrey,
        onPressed: () {
          _createPostDialog(context);
        },
        child: Icon(Icons.add),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.miniEndTop,
    );
  }

  Future<void> _createPostDialog(BuildContext context) async {
    String postText = '';
    String imageAsset = '';

    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Create a new post'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              TextField(
                decoration: InputDecoration(hintText: 'Enter your post text'),
                onChanged: (text) {
                  postText = text;
                },
              ),
              SizedBox(height: 10),
              InkWell(
                onTap: () async {
                  imageAsset = await _getImageFromGallery();
                  setState(() {});
                },
                child: Container(
                  height: 40,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.grey,
                    ),
                  ),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Center(
                    child: Text(
                      'Select Image',
                      style: TextStyle(color: Colors.grey),
                    ),
                  ),
                ),
              ),
              if (imageAsset.isNotEmpty)
                Image.network(
                  imageAsset,
                  width: double.infinity,
                  height: 200,
                  fit: BoxFit.cover,
                ),
            ],
          ),
          actions: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('Cancel'),
                ),
                TextButton(
                  onPressed: () {
                    setState(() {
                      posts.add(Post(
                        username: 'Your Username',
                        text: postText,
                        imageAsset: imageAsset,
                      ));
                      Navigator.of(context).pop();
                    });
                  },
                  child: Text('Post'),
                ),
              ],
            ),
          ],
        );
      },
    );
  }

  Future<String> _getImageFromGallery() async {
    final picker = ImagePicker();
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    if (pickedFile != null) {
      return pickedFile.path;
    }

    return '';
  }
}

class PostWidget extends StatefulWidget {
  final Post post;

  const PostWidget({Key? key, required this.post}) : super(key: key);

  @override
  _PostWidgetState createState() => _PostWidgetState();
}

class _PostWidgetState extends State<PostWidget> {
  bool isLiked = false;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage('assets/images/splash.png'),
            ),
            title: Text(widget.post.username),
            subtitle: Text('Posted 2 hours ago'),
            trailing: PopupMenuButton(
              itemBuilder: (context) => [
                PopupMenuItem(
                  child: Text('Edit Post'),
                  value: 'edit',
                ),
                PopupMenuItem(
                  child: Text('Delete Post'),
                  value: 'delete',
                ),
              ],
              onSelected: (value) {
                if (value == 'edit') {
                  _editPostDialog(context);
                } else if (value == 'delete') {
                  _deletePostDialog(context);
                }
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.all(10),
            child: Text(widget.post.text),
          ),
          if (widget.post.imageAsset.isNotEmpty)
            Image.network(
              widget.post.imageAsset,
              width: double.infinity,
              height: 200,
              fit: BoxFit.cover,
            ),
          Row(
            children: [
              IconButton(
                onPressed: () {
                  setState(() {
                    isLiked = !isLiked;
                    widget.post.likes += isLiked ? 1 : -1;
                  });
                },
                icon: Icon(
                  isLiked ? Icons.favorite : Icons.favorite_border,
                  color: isLiked ? Colors.red : Colors.grey,
                ),
              ),
              Text('${widget.post.likes} likes'),
              SizedBox(width: 130),
              TextButton(
                onPressed: () {
                  _addCommentDialog(context);
                },
                child: Text('Add Comment'),
              ),
            ],
          ),
          Divider(),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              for (Comment comment in widget.post.comments)
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  child: Text(
                    '${comment.username}: ${comment.text}',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
            ],
          ),
          Divider(),
        ],
      ),
    );
  }

  Future<void> _addCommentDialog(BuildContext context) async {
    String commentText = '';

    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Add a comment'),
          content: TextField(
            decoration: InputDecoration(hintText: 'Enter your comment'),
            onChanged: (text) {
              commentText = text;
            },
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                setState(() {
                  widget.post.comments.add(Comment(
                    username: 'Your Commenter',
                    text: commentText,
                  ));
                  Navigator.of(context).pop();
                });
              },
              child: Text('Comment'),
            ),
          ],
        );
      },
    );
  }

  Future<void> _editPostDialog(BuildContext context) async {
    String updatedText = widget.post.text;
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Edit your post'),
          content: TextField(
            decoration: InputDecoration(hintText: 'Edit your post'),
            onChanged: (text) {
              updatedText = text;
            },
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('Cancel'),
            ),
            TextButton(
              onPressed: () {
                setState(() {
                  widget.post.text = updatedText;
                  Navigator.of(context).pop();
                });
              },
              child: Text('Save'),
            ),
          ],
        );
      },
    );
  }

  Future<void> _deletePostDialog(BuildContext context) async {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Delete this post?'),
          content: Text('This action cannot be undone.'),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('Cancel'),
            ),
            TextButton(
              onPressed: () {
                setState(() {
                  var posts;
                  posts.remove(widget.post);
                  Navigator.of(context).pop();
                });
              },
              child: Text('Delete'),
            ),
          ],
        );
      },
    );
  }
}