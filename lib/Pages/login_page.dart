import 'dart:convert';
import 'package:hackathonhub/models/Utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hackathonhub/Pages/dashboard_page.dart';
import 'package:hackathonhub/Pages/forgot_page.dart';
import 'package:hackathonhub/main_button.dart';
import '../models/user.dart';
import 'signu_page.dart';
import 'package:hackathonhub/text_fild.dart';
import 'package:sign_in_button/sign_in_button.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:jwt_decode/jwt_decode.dart';

User? currentUser;

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController userEmail = TextEditingController();
  TextEditingController userPass = TextEditingController();
  bool isPasswordObscure = true; // Add this line to define the variable

  String? token;

  @override
  void initState() {
    super.initState();
    loadTokenFromSharedPreferences();
  }

  Future<void> loadTokenFromSharedPreferences() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String? savedToken = prefs.getString('token');

    if (savedToken != null) {
      setState(() {
        token = savedToken;
      });
    }
  }

  Future<void> saveTokenToSharedPreferences(String token) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('token', token);
  }

  Future<void> clearTokenFromSharedPreferences() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove('token');
  }

  handleLogin() {
    if (userEmail.text.isNotEmpty && userPass.text.isNotEmpty) {
      loginUser(userEmail.text, userPass.text);
    } else {
      Fluttertoast.showToast(msg: "All fields are required");
    }
  }

  Future<void> loginUser(String email, String password) async {
    try {
      final response = await http.post(
        Uri.parse('${ApiConstants.baseUrl}:4000/api/users/login'),
        body: jsonEncode({'Email': email, 'Password': password}),
        headers: {'Content-Type': 'application/json'},
      );

      if (response.statusCode == 201) {
        final Map<String, dynamic> data = jsonDecode(response.body);
        final String token = data['token'];
        // Save the token to SharedPreferences
        await saveTokenToSharedPreferences(token);
        setState(() {
          this.token = token;
        });

        print(token);

        final Map<String, dynamic> decodedToken = Jwt.parseJwt(token);
        int UserID = decodedToken['UserID'];
        String Email = decodedToken['Email'];

        fetchUserData(UserID);

        print(UserID);
        print(Email);

        Navigator.push(
            context, MaterialPageRoute(builder: (_) => DashboardPage()));
      } else {
        final Map<String, dynamic> error = jsonDecode(response.body);
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Login Failed'),
              content: Text(error['error']),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('OK'),
                ),
              ],
            );
          },
        );
      }
    } catch (e) {
      print('Error during login: $e');
    }
  }

  Future<void> fetchUserData(int UserID) async {
    final response = await http.get(
      Uri.parse('${ApiConstants.baseUrl}:4000/api/users/$UserID'),
    );

    if (response.statusCode == 201) {
      final Map<String, dynamic> userData = jsonDecode(response.body);
      currentUser = User.fromJson(userData["user"]);

      print(currentUser?.UserID);
      print(currentUser?.Email);
      print(currentUser?.Username);
      print(currentUser?.Password);
    } else {
      final Map<String, dynamic> error = jsonDecode(response.body);
      print(error["error"]);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [Colors.black, Colors.blueGrey],
        ),
      ),
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
            SizedBox(height: 100),
            Text(
              'Welcome Back!',
              style: TextStyle(
                fontFamily: 'Epilogue',
                fontSize: 40,
                color: Colors.white,
                fontWeight: FontWeight.w700,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              'Please sign in to your account',
              style: TextStyle(
                fontFamily: 'Epilogue',
                fontSize: 15,
                color: Colors.white,
                fontWeight: FontWeight.w400,
              ),
            ),
            SizedBox(
              height: 80.0,
            ),
            textFild(
              controller: userEmail,
              keyboardType: TextInputType.emailAddress,
              hintTxt: 'Email',
            ),
            textFild(
              controller: userPass,
              hintTxt: 'Password',
              isObscureText: isPasswordObscure,
              suffixIcon: (CupertinoIcons.eye_slash_fill), // Initial icon
              onToggleVisibility: (bool isVisible) {
                setState(() {
                  isPasswordObscure = isVisible;
                });
              },
            ),
            SizedBox(
              height: 10.0,
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: EdgeInsets.only(right: 20.0)
              ),
            ),
            SizedBox(
              height: 30.0,
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Column(
                children: [
                  Mainbutton(
                    onTap: handleLogin,
                    btnColor: Colors.lightBlueAccent.withOpacity(0.5),
                    text: 'Log in',
                  ),
                  SizedBox(
                    height: 260.0,
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (builder) => SignUpPage()));
                    },
                    child: RichText(
                        text: TextSpan(children: [
                      TextSpan(
                        text: 'Don\'t have an account? ',
                        style: TextStyle(
                          fontFamily: 'Epilogue',
                          fontSize: 12,
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      TextSpan(
                        text: ' Sign up',
                        style: TextStyle(
                          fontFamily: 'Epilogue',
                          fontSize: 12,
                          color: Colors.lightBlueAccent,
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    ])),
                  ),
                ],
              ),
            )
          ]),
        ),
      ),
    ));
  }
}
