import 'package:flutter/material.dart';
import 'package:hackathonhub/main_button.dart';
import 'signu_page.dart';
import 'package:hackathonhub/text_fild.dart';

class ForgotPage extends StatefulWidget {
  const ForgotPage({Key? key}) : super(key: key);

  @override
  _ForgotPageState createState() => _ForgotPageState();
}

class _ForgotPageState extends State<ForgotPage> {
  TextEditingController userEmail = TextEditingController();
  TextEditingController userPass = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Colors.black,
              Colors.blueGrey
            ],
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
            SizedBox(height: 100.0),
            Text(
              'Reset Password',
              style: TextStyle(
                fontFamily: 'Epilogue',
                fontSize: 40,
                color: Colors.white,
                fontWeight: FontWeight.w700,
              ),
            ),
            SizedBox(
              height: 60.0,
            ),
            textFild(
              controller: userEmail,
              keyboardType: TextInputType.emailAddress,
              hintTxt: 'Email',
            ),
            SizedBox(
              height: 10.0,
            ),
            Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: EdgeInsets.only(right: 20.0),
                )),
            SizedBox(
              height: 30.0,
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Column(
                children: [
                  Mainbutton(
                    onTap: () {},
                    text: 'Reset',
                    btnColor: Colors.lightBlueAccent.withOpacity(0.5),
                  ),
                  SizedBox(
                    height: 50.0,
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (builder) => SignUpPage()));
                    },
                    child: RichText(
                        text: TextSpan(children: [
                      TextSpan(
                        text: 'Don\'t have an account? ',
                        style: TextStyle(
                          fontFamily: 'Epilogue',
                          fontSize: 12,
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      TextSpan(
                        text: ' Sign up',
                        style: TextStyle(
                          fontFamily: 'Epilogue',
                          fontSize: 12,
                          color: Colors.lightBlueAccent,
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    ])),
                  ),
                ],
              ),
            )
          ]),
        ),
      ),
    );
  }
}
