import 'dart:convert';
import 'package:hackathonhub/models/Utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hackathonhub/Pages/login_page.dart';
import 'package:hackathonhub/main_button.dart';
import 'package:hackathonhub/text_fild.dart';
import 'package:sign_in_button/sign_in_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:hackathonhub/Pages/dashboard_page.dart';
import 'package:http/http.dart' as http;

class SignUpPage extends StatefulWidget {
  const SignUpPage({Key? key}) : super(key: key);
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  TextEditingController userName = TextEditingController();
  TextEditingController userPass = TextEditingController();
  TextEditingController userEmail = TextEditingController();
  bool isPasswordObscure = true;
  handleRegister() {
    if (userName.text.isNotEmpty &&
        userEmail.text.isNotEmpty &&
        userPass.text.isNotEmpty) {
      registerUser(userName.text, userEmail.text, userPass.text);
    } else {
      Fluttertoast.showToast(msg: "All fields are required");
    }
  }

  Future<void> registerUser(
      String username, String email, String password) async {
    try {
      final response = await http.post(
        Uri.parse('${ApiConstants.baseUrl}:4000/api/users/register'),
        body: jsonEncode(
            {'Username': username, 'Email': email, 'Password': password}),
        headers: {'Content-Type': 'application/json'},
      );

      if (response.statusCode == 201) {
        // Registration successful
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Registration Successful'),
              content: Text('You can now log in with your new account.'),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => LoginPage()));
                  },
                  child: Text('OK'),
                ),
              ],
            );
          },
        );
      } else {
        final Map<String, dynamic> error = jsonDecode(response.body);
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Registration Failed'),
              content: Text(error['error']),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('OK'),
                ),
              ],
            );
          },
        );
      }
    } catch (e) {
      print('Error during registration: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [Colors.black, Colors.blueGrey],
          ),
        ),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: 90.0),
                  Text(
                    'Create new account!',
                    style: TextStyle(
                      fontFamily: 'Epilogue',
                      fontSize: 30,
                      color: Colors.white,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Tell us about yourself',
                    style: TextStyle(
                      fontFamily: 'Epilogue',
                      fontSize: 15,
                      color: Colors.white,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  SizedBox(
                    height: 70.0,
                  ),
                  textFild(
                    controller: userName,
                    keyboardType: TextInputType.name,
                    hintTxt: 'User',
                  ),
                  textFild(
                    controller: userEmail,
                    keyboardType: TextInputType.emailAddress,
                    hintTxt: 'Email',
                  ),
                  textFild(
                    controller: userPass,
                    hintTxt: 'Password',
                    isObscureText: isPasswordObscure,
                    suffixIcon: (CupertinoIcons.eye_slash_fill), // Initial icon
                    onToggleVisibility: (bool isVisible) {
                      setState(() {
                        isPasswordObscure = isVisible;
                      });
                    },
                  ),
                  SizedBox(
                    height: 50.0,
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Column(
                      children: [
                        Mainbutton(
                          onTap: handleRegister,
                          btnColor: Colors.lightBlueAccent.withOpacity(0.5),
                          text: 'Sign Up',
                        ),
                        SizedBox(
                          height: 200.0,
                        ),
                        TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: RichText(
                              text: TextSpan(children: [
                            TextSpan(
                              text: 'Have an account? ',
                              style: TextStyle(
                                fontFamily: 'Epilogue',
                                fontSize: 12,
                                color: Colors.white,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            TextSpan(
                              text: ' Log In',
                              style: TextStyle(
                                fontFamily: 'Epilogue',
                                fontSize: 12,
                                color: Colors.lightBlueAccent,
                                fontWeight: FontWeight.w800,
                              ),
                            ),
                          ])),
                        ),
                      ],
                    ),
                  )
                ]),
          ),
        ),
      ),
    );
  }
}
